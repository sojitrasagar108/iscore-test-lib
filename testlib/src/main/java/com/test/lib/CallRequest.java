package com.test.lib;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest {

    public static String URL = Constant.BASIC_URL + Constant.URL_VERSION + "web_services/";

    public App app;
    public Context ct;
    public Fragment ft;

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }


    public void getLogin(String register_mobile, String guid, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "login?");
        map.put("register_mobile", register_mobile);
        map.put("imei_number", guid);
        map.put("device_id", "");
        map.put("player_id", PleyaerID);
        map.put("device_type", "android");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.GET, map);
    }


}
