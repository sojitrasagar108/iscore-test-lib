package com.test.lib;

import android.os.Environment;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {
    public static int EXTERNAL_STORAGE_PERMISSION = 9999;

    public static String LOCAL_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/.iScore";

    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;


   public static String BASIC_URL = "https://test.pemiscore.parshvaa.com/web_services/";
    public static String URL_VERSION = "version_39/";


    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, POST_WITH_JSON, Eval_POST_WITH_JSON;
    }


    public enum REQUESTS {
        getLogin
    }
}
