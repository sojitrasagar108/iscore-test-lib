package com.test.lib;

import android.content.Context;
import android.os.AsyncTask;

import com.test.lib.interfaces.GlobalResponceListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by Karan - Empiere on 6/26/2018.
 */

public class LoginClass implements AsynchTaskListner {
    public MainUser mainObj;
    public JsonParserUniversal jParser;
    public Context context;
    public GlobalResponceListner resListner;


    public void IntilizeLogin(String Mobileno, Context context) {

        resListner = (GlobalResponceListner) context;

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", Constant.BASIC_URL + Constant.URL_VERSION + "web_services/login?");
        map.put("register_mobile", Mobileno);
        map.put("imei_number", "");
        map.put("device_id", "");
        map.put("player_id", "d418e123-55cf-4cf3-a403-728ffe551694");
        map.put("device_type", "android");
           new MyRequest(context).execute(map);


    }

    public LoginClass() {
        jParser = new JsonParserUniversal();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

    }

    public class MyRequest extends AsyncTask<Map<String, String>, MainUser, String> {


        Context context;
        public App app;
        String examTypesID = "";

        private MyRequest(Context context) {
            this.context = context;
            app = App.getInstance();

        }

        @Override
        protected void onPreExecute() {
            examTypesID = "";
            Utils.showProgressDialog(context);
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Map<String, String>... map) {

            /*Utils.SystemOutPrintln("new Requested URL >> ", map[0].get("url"));
            HttpPost postRequest = new HttpPost(map[0].get("url"));
            DefaultHttpClient httpclient = getNewHttpClient();
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            for (String key : map[0].keySet()) {
                Utils.SystemOutPrintln(key, "====>" + map[0].get(key));
                nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
            }
            try {
                postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Utils.SystemOutPrintln("Final URI::", postRequest.getEntity().toString());
            HttpResponse response = null;
            try {
                response = httpclient.execute(postRequest);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity httpEntity = response.getEntity();
            String responseBody = null;
            try {
                responseBody = EntityUtils.toString(httpEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseBody;*/
            String responseBody = "";
            DefaultHttpClient httpClient = getNewHttpClient();
            String query = map[0].get("url");
            //Utils.SystemOutPrintln();
            map[0].remove("url");


            List<String> values = new ArrayList<String>(map[0].values());


            List<String> keys = new ArrayList<String>(map[0].keySet());

            for (int i = 0; i < values.size(); i++) {
                //Utils.SystemOutPrintln();

                Utils.SystemOutPrintln(keys.get(i), "====>" + values.get(i));
                query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");

                if (i < values.size() - 1) {
                    query += "&";
                }
            }
            // URLEncoder.encode(query, "UTF-8");
            Utils.SystemOutPrintln("URL", "====>" + query);
            HttpGet httpGet = new HttpGet(query);

            HttpResponse httpResponse = null;
            try {
                httpResponse = httpClient.execute(httpGet);

                HttpEntity httpEntity = httpResponse.getEntity();
                responseBody = EntityUtils.toString(httpEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Utils.SystemOutPrintln("StatusCode", "====>" + httpResponse.getStatusLine().getStatusCode());
            Utils.SystemOutPrintln("Responce", "====>" + responseBody);
            return responseBody;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Utils.hideProgressDialog();
            Utils.Log("RESPONSE :-> ", s);
            JSONObject jObj = null;
            try {
                jObj = new JSONObject(s);

                if (jObj.getBoolean("status") == true) {
                    JSONObject jData = jObj.getJSONObject("data");
                    mainObj = (MainUser) jParser.parseJson(jData, new MainUser());


                    resListner.onLoginSuccesfull(mainObj);

                }

            } catch (JSONException e)

            {
                e.printStackTrace();
            }
        }

    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}